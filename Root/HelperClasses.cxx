/* **********************************************************************\
 *                                                                      *
 *  #   Name:   HelperClasses                                           *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 05/03/16 First version              J. Bossio (jbossios@cern.ch)  *
\************************************************************************/

#include "easyPlot/HelperClasses.h"

//---------------------------
// iBaseObject constructors
//---------------------------

HelperClasses::iBaseObject::iBaseObject(TString id){
  m_ID = id;
}

HelperClasses::iBaseObject::iBaseObject(TString id, TFile* file, TString name){
  m_ID   = id;
  m_File = file;
  m_Name = name;
}

//---------------------------
// Line constructor
//---------------------------

HelperClasses::iLine::iLine(TString axis, double min, double max, double value, int color){
 m_Axis  = axis;
 m_Min   = min;
 m_Max   = max;
 m_Value = value;
 m_Color = color;
}

//---------------------------
// iHist constructors
//---------------------------

HelperClasses::iHist::iHist(TString id, TFile* file, TString name) : HelperClasses::iBaseObject(id,file,name){
  this->setLegend(id);
}

HelperClasses::iHist::iHist(TString id, TH1D* hist) : HelperClasses::iBaseObject(id){
 m_Hist   = hist;
 this->setLegend(id);
}

//---------------------------
// iGraph constructors
//---------------------------

HelperClasses::iGraph::iGraph(TString id, TFile* file, TString name) : HelperClasses::iBaseObject(id,file,name){
 this->setLegend(id);
}

HelperClasses::iGraph::iGraph(TString id, TGraph* graph) : HelperClasses::iBaseObject(id){
 m_Graph  = graph;
 this->setLegend(id);
}

//---------------------------
// iLegend constructor
//---------------------------

HelperClasses::iLegend::iLegend(double x, double y, TString text){
 m_X    = x;
 m_Y    = y;
 m_Text = text;
}

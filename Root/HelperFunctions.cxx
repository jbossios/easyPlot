/* **********************************************************************\
 *                                                                      *
 *  #   Name:   HelperFunctions                                         *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 05/03/16 First version              J. Bossio (jbossios@cern.ch)  *
\************************************************************************/

#include "easyPlot/HelperFunctions.h"
#include "easyPlot/HelperClasses.h"

using namespace std;

void HelperFunctions::SetxRange(TH1D* h, double xmin, double xmax){

  if(xmin!=xmax){
    double hxmin = h->GetXaxis()->GetBinLowEdge(h->FindBin(xmin));
    double hxmax = h->GetXaxis()->GetBinUpEdge(h->FindBin(xmax));
    h->SetAxisRange(hxmin,hxmax,"X");
  }

}

void HelperFunctions::SetxRange(TGraph* g, double xmin, double xmax){

  if(xmin!=xmax){
    auto Xaxis = g->GetXaxis();
    Xaxis->SetLimits(xmin,xmax);
  }

}

void HelperFunctions::SetxRangeNew(TH1D* h, double& xmin, double& xmax){

  if(xmin!=xmax){
    double hxmin = h->GetXaxis()->GetBinLowEdge(h->FindBin(xmin));
    double hxmax = h->GetXaxis()->GetBinUpEdge(h->FindBin(xmax));
    h->SetAxisRange(hxmin,hxmax,"X");
    if(hxmin>xmin) xmin = hxmin;
    if(hxmax>xmax) xmax = hxmax;
  }

}

void HelperFunctions::SetgStyle(){

  gStyle->SetLegendBorderSize(0);
  gStyle->SetCanvasColor(-1); 
  gStyle->SetPadColor(-1); 
  gStyle->SetFrameFillColor(-1); 
  gStyle->SetHistFillColor(-1); 
  gStyle->SetTitleFillColor(-1); 
  gStyle->SetFillColor(-1); 
  gStyle->SetFillStyle(4000); 
  gStyle->SetStatStyle(0); 
  gStyle->SetTitleStyle(0); 
  gStyle->SetCanvasBorderSize(0); 
  gStyle->SetFrameBorderSize(0); 
  gStyle->SetStatBorderSize(0); 
  gStyle->SetTitleBorderSize(0);
  gStyle->SetLegendBorderSize(0);

}

void HelperFunctions::Rebin(TH1D* h, int rebin){
  if(rebin!=0) h->Rebin(rebin);
}

void HelperFunctions::SetTitles(THStack* hs, TString xTitle, TString yTitle, double xOffset, double yOffset){
 
  if(xTitle!=""){
    hs->GetXaxis()->SetTitle(xTitle);
    if(xOffset!=-1) hs->GetXaxis()->SetTitleOffset(xOffset);
  }

  if(yTitle!=""){
    hs->GetYaxis()->SetTitle(yTitle);
    hs->GetYaxis()->SetTitleSize(20);
    hs->GetYaxis()->SetTitleFont(43);
    if(yOffset!=-1) hs->GetYaxis()->SetTitleOffset(yOffset);
  }

}

void HelperFunctions::SetyRange(TH1D* h, double ymin, double ymax){
 
  if(ymin!=ymax){
    h->SetMinimum(ymin);
    h->SetMaximum(ymax);
  }

}

void HelperFunctions::SetyRange(TGraphAsymmErrors* g, double ymin, double ymax){

  if(ymin!=ymax){
    g->SetMinimum(ymin);
    g->SetMaximum(ymax);
  }

}

void HelperFunctions::SetTitlesSecondPanel(THStack* hs, TString title, bool moreLabelsX){

  // Y axis ratio plot settings
  hs->GetYaxis()->SetTitle(title);
  hs->GetYaxis()->SetTitleSize(20);
  hs->GetYaxis()->SetTitleFont(43);
  hs->GetYaxis()->SetTitleOffset(1.35);
  hs->GetYaxis()->SetLabelFont(43);
  hs->GetYaxis()->SetLabelSize(16);

  // X axis ratio plot settings
  hs->GetXaxis()->SetTitleSize(20);
  hs->GetXaxis()->SetTitleFont(43);
  hs->GetXaxis()->SetTitleOffset(3.5);
  if(moreLabelsX){
    hs->GetXaxis()->SetLabelFont(42);
    hs->GetXaxis()->SetLabelSize(0.09);
  } else{
    hs->GetXaxis()->SetLabelFont(43);
    hs->GetXaxis()->SetLabelSize(16);
  }
  
}

int HelperFunctions::DrawLine(TH1D* h, TString axis, double min, double max, double value, int color){
  
  double nbins;
  double minimum = min;
  double maximum = max;
  if(min==max){
    if(axis=="Y" || axis=="y"){
      nbins = h->GetNbinsX();
      minimum = h->GetXaxis()->GetBinLowEdge(1);
      maximum = h->GetXaxis()->GetBinUpEdge(nbins);
    } else if(axis=="X" || axis=="x"){
      nbins = h->GetNbinsY();
      minimum = h->GetYaxis()->GetBinLowEdge(1);
      maximum = h->GetYaxis()->GetBinUpEdge(nbins);
    }
  }

  TLine* line = nullptr;
  if(axis=="Y" || axis=="y") line = new TLine(minimum, value, maximum, value);
  else if(axis=="X" || axis=="x") line = new TLine(value, minimum, value, maximum);
  if(line!=nullptr){ // protection
    line->SetLineStyle(2);
    if(color!=-1) line->SetLineColor(color);
    line->Draw("same");
    return 1;
  } else {
    cout << "ERROR: axis type for DrawLine() not recognize, exiting" << endl;
    return 0;
  }

  return 1;

}

void HelperFunctions::DivideBinbyBin(TH1D* h){

  Int_t nbinsx = h->GetNbinsX();
  for(int i=1;i<=nbinsx;i++){
     if(h->GetBinContent(i)!=0) h->SetBinContent( i, h->GetBinContent(i)/h->GetBinWidth(i) );
     if(h->GetBinError(i)!=0)   h->SetBinError( i, h->GetBinError(i)/h->GetBinWidth(i) );
  }

}

int HelperFunctions::DrawLegends(vector<TH1D*> hists, vector<TString> legends, vector<TString> drawOptions, bool secondPanel, TString luminosity, TString cme, TString atlasLabel, TString position, bool hErrors, vector<HelperClasses::iLegend*> texts, bool notlegends, double legendX1, double legendY1, double legendX2, double legendY2){

  bool showLumi = false;
  if(luminosity!="-999") showLumi = true;
  bool showCME = false;
  if(cme!="-999") showCME = true;

  double top = 0.86;
  double step = 0.04;
  if(!secondPanel){
    top = 0.88;
    step = 0.03;
  }

  TString scaling_text_1 = "1.3";
  TString scaling_text_2 = "1.4";
  TString scaling_text_3 = "1.0";
  if(!secondPanel){
    scaling_text_1 = "1";
    scaling_text_2 = "1.1";
    scaling_text_3 = "0.8";
  }

  // ATLAS label
  TString text1 = "";
  double xshift1 = 0.; // shift when legend is on the right
  if(atlasLabel == "Internal"){
    text1   = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_2+"]{#font[72]{ATLAS} #font[42]{Internal}}}";
    xshift1 = 0.13;
  } else if (atlasLabel == "Preliminary"){
    text1 = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_2+"]{#font[72]{ATLAS} #font[42]{Preliminary}}}";
    xshift1 = 0.18;
  } else if (atlasLabel == "ATLAS"){
    text1 = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_2+"]{#font[72]{ATLAS}}}";
  } else if (atlasLabel == "WIP"){
    text1 = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_2+"]{#font[72]{ATLAS} #font[42]{Work in Progress}}}";
    xshift1 = 0.28;
  } else if (atlasLabel == "Simulation"){
    text1 = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_2+"]{#font[72]{ATLAS} #font[42]{Simulation}}}";
    xshift1 = 0.175;
  } else if (atlasLabel == "SimulationInternal"){
    text1 = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_2+"]{#font[72]{ATLAS} #font[42]{Simulation Internal}}}";
    xshift1 = 0.3;
  } else if (atlasLabel == "SimulationWIP"){
    text1 = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_2+"]{#font[72]{ATLAS} #font[42]{Simulation Work in Progress}}}";
    xshift1 = 0.45;
  } else if (atlasLabel == "SimulationPreliminary"){
    text1 = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_2+"]{#font[72]{ATLAS} #font[42]{Simulation Preliminary}}}";
    xshift1 = 0.35;
  }

  // Luminosity and CME
  TString text2 = "";
  double xshift2 = 0.; // shift when legend is on the right
  if(showLumi && showCME){
    text2 = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_3+"]{#int L dt = "+luminosity+" fb^{-1}, #sqrt{s} = "+cme+"TeV}}";
    xshift2 = 0.23;
  } else if(showLumi && !showCME){
    text2 = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_3+"]{#int L dt = "+luminosity+" fb^{-1}}}";
    xshift2 = 0.09;
  } else if(!showLumi && showCME){
    text2 = "#scale[" + scaling_text_1 + "]{#scale["+scaling_text_3+"]{#sqrt{s} = "+cme+"TeV}}";
  }
 
  TLatex *TextBlock1; 
  TLatex *TextBlock2; 

  // Set location of ATLAS label
  double y_1 = top;          // Default on top (Top, TopLeft or TopRight)
  double y_2 = top - 3*step; // Default on top (Top, TopLeft or TopRight)
  if(position == "Bottom") {
    y_1 = top - 0.58;
    y_2 = top - 0.58 - 3*step;
  }
  if(!showLumi){ // Put close CME to ATLAS lebel
    if(position.Contains("Top")) y_2 += step;
    if(position == "Bottom")     y_1 -= step;
  }
  if(secondPanel){
    if(position!="Bottom"){
      y_1 -= step;
      y_2 -= 1.75*step;
    } else {
      y_1 -= 0.5*step;
      y_2 -= 1.75*0.5*step;
    }
  }
  double x_1 = 0.8  + secondPanel*0.01; // Default on right
  double x_2 = 0.79 + secondPanel*0.01; // Default on right
  if(position == "Bottom" || position.Contains("Left") || position == "Top"){
    x_1 = 0.20;
    x_2 = 0.20;
  } else { // TopRight
    x_1 -= xshift1;
    x_2 -= xshift2;
  }
  if(!showLumi && !showCME && position=="Bottom") TextBlock1 = new TLatex(x_1,y_2,text1.Data());
  else{TextBlock1 = new TLatex(x_1,y_1,text1.Data());}
  if(text1!="") TextBlock2 = new TLatex(x_2,y_2,text2.Data()); // together with an ATLAS label
  else TextBlock2 = new TLatex(x_2,y_1,text2.Data()); // without an ATLAS label

  if(text1!="") TextBlock1->SetNDC(); // show no ATLAS label
  if(showLumi || showCME) TextBlock2->SetNDC();
  if(text1!="") TextBlock1->Draw("same"); // show no ATLAS label
  if(showLumi || showCME) TextBlock2->Draw("same");

  // Show additional (requested) text
  for(unsigned int i=0;i<texts.size();++i){
    TString additionalText = "#scale[1.3]{"+texts.at(i)->getText()+"}";
    if(!secondPanel) additionalText = "#scale[0.8]{"+texts.at(i)->getText()+"}";
    TLatex *TextBlock = new TLatex(texts.at(i)->getX(),texts.at(i)->getY(),additionalText);
    TextBlock->SetNDC();
    TextBlock->Draw("same");
  }

  // Number of histograms
  int nHists = hists.size();
  // take into account the number of histograms with NOLEGEND option
  int nHists_nolegend = 0;
  for(unsigned int i=1;i<legends.size();++i){
    if(legends.at(i)=="NOLEGEND") ++nHists_nolegend;
  }
  // Final number of histograms with legends
  nHists = nHists - nHists_nolegend;
  // Maximum length of legends
  int maxLength = HelperFunctions::GetLength(legends.at(0));
  for(unsigned int i=1;i<legends.size();++i){
    if(HelperFunctions::GetLength(legends.at(i))>maxLength && legends.at(i)!="NOLEGEND") maxLength = GetLength(legends.at(i));
  }
  if(maxLength<6) maxLength = 6;

  // Set position of legends
  double x1, x2, y1, y2;
  double dx = 0.017*maxLength;
  double dy = 0.008*6*nHists;
  if(position == "Top"){
    x2 = 0.92;
    (!secondPanel) ? y2 = 0.925 : y2 = 0.9;
    x1 = x2 - dx;
    (!secondPanel) ? y1 = y2 - dy : y1 = y2 - dy - 0.1;
  } else if(position == "Bottom"){
    x2 = 0.92;
    y1 = 0.18 - 0.1*secondPanel;
    x1 = x2 - dx;
    (!secondPanel) ? y2 = y1 + dy : y2 = y1 + dy + 0.15;
  } else if(position == "TopRight"){
    x2 = 0.92;
    y2 = y_2 - step - 0.01*secondPanel;
    x1 = x2 - dx;
    (!secondPanel) ? y1 = y2 - dy: y1 = y2 - dy -0.1;
  } else if(position == "TopLeft"){
    x1 = 0.20;
    y2 = y_2 - step - 0.01*secondPanel;
    x2 = x1 + dx;
    (!secondPanel) ? y1 = y2 - dy: y1 = y2 - dy -0.1;
  } else {
    cout << "Legend Position not recognised, exiting" << endl;
    return 0;
  }
  
  if(!showLumi && !showCME && position!="Top" && position!="Bottom") {
    y1 += 3*step;
    y2 += 3*step;
  }

  if(legendX1!=-1 && legendY1!=-1 && legendX2!=-1 && legendY2!=-1){
    x1 = legendX1;
    x2 = legendX2;
    y1 = legendY1;
    y2 = legendY2;
  }

  if(!notlegends){// Draw TLegends
    TLegend *legend = new TLegend(x1,y1,x2,y2);
    legend->SetTextFont(42);
    // Loop over histograms
    for(unsigned int i=0;i<hists.size();++i){
      if(legends.at(i)!="NOLEGEND"){
        if(drawOptions.at(i).Contains("hist") || drawOptions.at(i).Contains("HIST") || drawOptions.at(i).Contains("C") || drawOptions.at(i).Contains("c") || drawOptions.at(i).Contains("L") || drawOptions.at(i).Contains("l")) legend->AddEntry(hists.at(i), "#scale[0.9]{"+legends.at(i)+"}","L");
        else if(!hErrors){legend->AddEntry(hists.at(i), "#scale[0.9]{"+legends.at(i)+"}","P");}
        else{legend->AddEntry(hists.at(i), "#scale[0.9]{"+legends.at(i)+"}","PL");}
      }
    }
    legend->Draw("same");
  }

  return 1;
}

int HelperFunctions::GetLength(TString string){
  int length = string.Length();
  int count = 0;
  for (Int_t i=0; i<string.Length(); ++i){
    if ( string[i] == TString("#") ) count++;
    if ( string[i] == TString("{") ) count++;
    if ( string[i] == TString("}") ) count++;
    if ( string[i] == TString("_") ) count++;
    if ( string[i] == TString("^") ) count++;
  }
  return length-count;
}

/* **********************************************************************\
 *                                                                      *
 *  #   Name:   Plotter1D                                               *
 *                                                                      *
 *  #   Date    Comments                   By                           *
 * -- -------- -------------------------- ----------------------------- *
 *  1 04/11/17 First version              J. Bossio (jbossios@cern.ch)  *
\************************************************************************/

#include "easyPlot/Plotter1D.h"
#include "easyPlot/HelperFunctions.h"

using namespace HelperClasses;
using namespace HelperFunctions;
using namespace std;

//--------------
// Constructors
//--------------

Plotter1D::Plotter1D(TString outName){
  m_OutputName = outName;
}

Plotter1D::Plotter1D(){ // Useful to set common format for other instances of Plotter1D 
}

//------------
// Destructor
//------------

Plotter1D::~Plotter1D(){
  for(unsigned int i=0;i<m_Hists.size();++i) delete m_Hists.at(i);
  for(unsigned int i=0;i<m_Lines.size();++i) delete m_Lines.at(i);
  for(unsigned int i=0;i<m_LinesSecondPanel.size();++i) delete m_LinesSecondPanel.at(i);
  for(unsigned int i=0;i<m_Texts.size();++i) delete m_Texts.at(i);
  if(m_Stack!=nullptr)    delete m_Stack;
  if(m_Stack_SP!=nullptr) delete m_Stack_SP;
}

//---------------
// MAIN FUNCTION
//---------------

int Plotter1D::Write(){

  if(m_Debug) cout << "Inside Plotter::Write()" << endl;
  
  if(!m_Debug) gErrorIgnoreLevel = kWarning; // Suppress INFO messages

  // Number of provided histograms/graphs
  m_Nhists  = m_Hists.size();
  m_Ngraphs = m_Graphs.size();

  // Protections
  if(!passProtections()) return 0;

  // Choose first added histogram as refence when not reference specified
  if(m_ComparisonType!="Normal" && m_ReferenceHist=="") m_ReferenceHist = m_Hists.at(0)->ID();

  // Disable Horizonal Errors when requested
  if(!m_HorizontalErrors) gStyle->SetErrorX(0); // ATLAS recommendation (default)
  else gStyle->SetErrorX(0.5);

  // Find number of the reference histogram/graph
  bool referenceFound = findReferenceHist();
  if(!referenceFound && m_ComparisonType!="Normal"){
    cout << "PlotterID::Write(): Provided Reference for the second panel does not match to any histogram ID, exiting" << endl;
    return 0;
  }

  // Read provided options for each histogram
  readOptions();

  // More Protections
  if(!passMoreProtections()) return 0;

  //------------------
  // Canvas and TPad
  //------------------

  TCanvas *can = new TCanvas();

  // Output Figure
  TString outPlot = m_OutputName;
  if(m_OutputFolder!="NONE"){
    outPlot.Insert(0,"/");
    outPlot.Insert(0,m_OutputFolder);
  }

  // Format
  bool Cformat = false;
  if(m_OutFormat == "PDF")      outPlot += ".pdf";
  else if(m_OutFormat == "PNG") outPlot += ".png";
  else if(m_OutFormat == "EPS") outPlot += ".eps";
  else if(m_OutFormat == "C"){
   outPlot += ".C";
   Cformat = true;
  }
  if(!Cformat) can->Print(outPlot+"[");

  // TPad for upper plot
  TPad *pad1 = new TPad("pad1", "pad1", 0, 0.4, 1, 1.0);
  if(m_ComparisonType != "Normal" && !m_ComparisonType.Contains("Only") ){ // second pad requested
    m_SecondPanel = true;
    pad1->SetTopMargin(0.08);
    pad1->SetBottomMargin(0.03); // Upper and lower plot are joined
    pad1->Draw();                // Draw the upper pad: pad1
    pad1->cd();                  // pad1 becomes the current pad
  }

  //----------------
  // Get histograms
  //----------------
  if(m_Debug) cout << "Get histograms" << endl;
  vTh1 Hists;
  if(!GetInputs<iHist,TH1D>(m_Hists,Hists)) return 0;
  if(m_Debug) cout << "Histograms succesfully retrieved" << endl;

  // Remove underflow and overflow bins from histograms
  for(unsigned int i=0;i<m_Nhists;++i){
    if(m_Debug) cout << "Remove underflow and overflow bins" << endl;
    if(Hists.at(i)->GetBinContent(0)!=0) Hists.at(i)->SetBinContent(0,0);
    if(Hists.at(i)->GetBinContent(Hists.at(i)->GetNbinsX()+1)!=0) Hists.at(i)->SetBinContent(Hists.at(i)->GetNbinsX()+1,0);
  }

  //----------------
  // Get graphs
  //----------------
  if(m_Debug) cout << "Get graphs" << endl;
  vGph Graphs;
  if(!GetInputs<iGraph,TGraph>(m_Graphs,Graphs)) return 0;
  if(m_Debug) cout << "Graphs succesfully retrieved" << endl;

  // If x-axis range was not provided it will be automatically chosen
  if(m_Xmin==m_Xmax){
    if(m_Debug) cout << "Setting x-axis range automatically" << endl;
    if(m_Nhists>0){
      m_Xmin = Hists.at(0)->GetXaxis()->GetBinLowEdge(1);
      m_Xmax = Hists.at(0)->GetXaxis()->GetBinUpEdge(Hists.at(0)->GetNbinsX());
    } else if(m_Ngraphs>0){
      m_Xmin = Graphs.at(0)->GetHistogram()->GetXaxis()->GetBinLowEdge(1);
      m_Xmax = Graphs.at(0)->GetHistogram()->GetXaxis()->GetBinUpEdge(Graphs.at(0)->GetHistogram()->GetNbinsX());
    }
    for(unsigned int i=0;i<m_Nhists;++i){// loop over hists
      double hxmin = Hists.at(i)->GetXaxis()->GetBinLowEdge(1);
      if(hxmin < m_Xmin) m_Xmin = hxmin;
      double hxmax = Hists.at(i)->GetXaxis()->GetBinUpEdge(Hists.at(i)->GetNbinsX());
      if(hxmax > m_Xmax) m_Xmax = hxmax;
    }
    for(unsigned int i=0;i<m_Ngraphs;++i){// loop over graphs
      double gxmin = Graphs.at(i)->GetHistogram()->GetXaxis()->GetBinLowEdge(1);
      if(gxmin < m_Xmin) m_Xmin = gxmin;
      double gxmax = Graphs.at(i)->GetHistogram()->GetXaxis()->GetBinUpEdge(Graphs.at(i)->GetHistogram()->GetNbinsX());
      if(gxmax > m_Xmax) m_Xmax = gxmax;
    }
  }

  //-----------------------------
  // Set Histograms' properties
  //-----------------------------

  if(m_Debug) cout << "Loop over histograms (set properties)" << endl;
  int colorCounter = 0;
  for(unsigned int i=0;i<m_Nhists;++i){

    // Rebin
    if(m_Hists.at(i)->ReBin()!=1) HelperFunctions::Rebin(Hists.at(i),m_Hists.at(i)->ReBin());

    // Divide bins by bin width
    if(m_Hists.at(i)->DivideBinByBinWidth()) HelperFunctions::DivideBinbyBin(Hists.at(i));

    // Smooth
    if(m_Hists.at(i)->Smooth()!=0) Hists.at(i)->Smooth(m_Hists.at(i)->Smooth());

    // Set x-axis range
    if(i==0) HelperFunctions::SetxRangeNew(Hists.at(i),m_Xmin,m_Xmax);
    else{HelperFunctions::SetxRange(Hists.at(i),m_Xmin,m_Xmax);}

    // Normalization and Scaling
    if(m_Hists.at(i)->NormType()!="NONE"){
      if(m_Hists.at(i)->NormType()=="Factor") Hists.at(i)->Scale(m_Hists.at(i)->NormFactor());
      else if(m_Hists.at(i)->NormType()=="Integral") Hists.at(i)->Scale(1./Hists.at(i)->Integral());
    }
    if(m_Hists.at(i)->ScaleFactor()!=1.){
      Hists.at(i)->Scale(m_Hists.at(i)->ScaleFactor());
    }
    
    // Cosmetics

    // Marker size
    if(i<5)       Hists.at(i)->SetMarkerSize(0.7);
    else if(i==5) Hists.at(i)->SetMarkerSize(0.9);
    else if(i==6) Hists.at(i)->SetMarkerSize(0.8);
    // Line and Marker colors
    if(m_Nhists<3){
      if(m_Nhists==1){
        if(m_Hists.at(i)->Color()!=-1){ // user color
          Hists.at(i)->SetMarkerColor(m_Hists.at(i)->Color());
          Hists.at(i)->SetLineColor(m_Hists.at(i)->Color());
        } else { // default color
          Hists.at(i)->SetMarkerColor(kBlack);
          Hists.at(i)->SetLineColor(kBlack);
        }
        Hists.at(i)->SetLineWidth(m_Hists.at(i)->LineWidth());
        Hists.at(i)->SetLineStyle(m_Hists.at(i)->LineStyle());
        Hists.at(i)->SetMarkerStyle(20);
        Hists.at(i)->SetFillStyle(0);
      } else { // m_Nhists==2
        bool useCircles = true;
        for(unsigned int j=0;j<m_Nhists;++j){
          if(m_Hists.at(j)->DrawOption().Contains("HIST") || m_Hists.at(j)->DrawOption().Contains("hist") || m_Hists.at(j)->DrawOption().Contains("C") || m_Hists.at(j)->DrawOption().Contains("L")) useCircles = false;
        }
        if(!useCircles){
          if(m_Hists.at(i)->Color()!=-1){ // user color
            Hists.at(i)->SetMarkerColor(m_Hists.at(i)->Color());
            Hists.at(i)->SetLineColor(m_Hists.at(i)->Color());
          } else { // default color
            Hists.at(i)->SetMarkerColor(m_DefaultColors.at(colorCounter));
            Hists.at(i)->SetLineColor(m_DefaultColors.at(colorCounter));
            colorCounter++;
          }
          Hists.at(i)->SetLineWidth(m_Hists.at(i)->LineWidth());
          Hists.at(i)->SetLineStyle(m_Hists.at(i)->LineStyle());
          Hists.at(i)->SetFillStyle(0);
        } else { // Full and Blank circles
          if(m_Hists.at(i)->Color()!=-1){ // user color
            (i==0) ? Hists.at(i)->SetMarkerColor(m_Hists.at(i)->Color()) : Hists.at(i)->SetMarkerColor(m_Hists.at(i)->Color());
            (i==0) ? Hists.at(i)->SetLineColor(m_Hists.at(i)->Color()) : Hists.at(i)->SetLineColor(m_Hists.at(i)->Color());
          } else { // default color
            (i==0) ? Hists.at(i)->SetMarkerColor(kBlack) : Hists.at(i)->SetMarkerColor(kBlack);
            (i==0) ? Hists.at(i)->SetLineColor(kBlack) : Hists.at(i)->SetLineColor(kBlack);
            colorCounter++;
          }
          (i==0) ? Hists.at(i)->SetMarkerStyle(20) : Hists.at(i)->SetMarkerStyle(24);
          Hists.at(i)->SetFillStyle(0);
        }
      }
    } else { // m_Nhists > 2
      if(m_Hists.at(i)->Color()!=-1){ // user color
        Hists.at(i)->SetMarkerColor(m_Hists.at(i)->Color());
        Hists.at(i)->SetLineColor(m_Hists.at(i)->Color());
      } else {
        Hists.at(i)->SetMarkerColor(m_DefaultColors.at(colorCounter));
        Hists.at(i)->SetLineColor(m_DefaultColors.at(colorCounter));
        colorCounter++;
      }
      bool useMarkers = true;
      for(unsigned int j=0;j<m_Nhists;++j){
        if(m_Hists.at(j)->DrawOption().Contains("HIST") || m_Hists.at(j)->DrawOption().Contains("hist") || m_Hists.at(j)->DrawOption().Contains("C") || m_Hists.at(j)->DrawOption().Contains("L")) useMarkers = false;
      }
      if(!useMarkers){
        Hists.at(i)->SetLineWidth(m_Hists.at(i)->LineWidth());
        Hists.at(i)->SetLineStyle(m_Hists.at(i)->LineStyle());
        Hists.at(i)->SetFillStyle(0);
      } else { // user markers
        Hists.at(i)->SetLineWidth(m_Hists.at(i)->LineWidth());
        Hists.at(i)->SetMarkerStyle(m_Markers.at(i));
      }
    }

    // Check if binning is the same as the previos histogram
    if(i>0 && m_ComparisonType!="Normal"){ // Protection when operating with the histograms
      if(Hists.at(i-1)->GetNbinsX()!=Hists.at(i)->GetNbinsX()){
        cout << "Plotter1D::Write(): Histograms have different binning, please check, exiting" << endl;
        return 0;
      }
      for(int ibin=1;ibin<=Hists.at(i)->GetNbinsX()+1;++ibin){
        if(Hists.at(i)->GetBinLowEdge(ibin)!=Hists.at(i-1)->GetBinLowEdge(ibin)){
          cout << "Plotter1D::Write(): Histograms have different binning, please check, exiting" << endl;
          return 0;
        }
      }
    }

  } //END: loop over hists
  if(m_Debug) cout << "End setting histograms's user options" << endl;

  /*-----------------------------
  // Set Graphs' properties
  //-----------------------------
  
  if(_Debug) cout << "Loop over graphs (set properties)" << endl;
  for(unsigned int i=0;i<_Ngraphs;++i){

    // Set x-axis range
    HelperFunctions::SetxRange(Graphs.at(i),_Xmin,_Xmax);

    // Cosmetics FIXME CONTINUE FROM HERE

    // Marker size
    if(i<5)Hists.at(i)->SetMarkerSize(0.7);
    else if(i==5) Hists.at(i)->SetMarkerSize(0.9);
    else if(i==6) Hists.at(i)->SetMarkerSize(0.8);
    // Line and Marker colors
    if(_Nhists<3){
      if(_Nhists==1){
        if(_Hists.at(i)->_color!=-1){ // user color
          Hists.at(i)->SetMarkerColor(_Hists.at(i)->_color);
          Hists.at(i)->SetLineColor(_Hists.at(i)->_color);
        } else { // default color
          Hists.at(i)->SetMarkerColor(kBlack);
          Hists.at(i)->SetLineColor(kBlack);
        }
        Hists.at(i)->SetLineWidth(_Hists.at(i)->_lineWidth);
        Hists.at(i)->SetLineStyle(_Hists.at(i)->_lineStyle);
        Hists.at(i)->SetMarkerStyle(20);
        Hists.at(i)->SetFillStyle(0);
      } else { // _Nhists==2
        bool useCircles = true;
        for(unsigned int j=0;j<_Nhists;++j){
          if(_Hists.at(j)->_drawOption.Contains("HIST") || _Hists.at(j)->_drawOption.Contains("hist") || _Hists.at(j)->_drawOption.Contains("C") || _Hists.at(j)->_drawOption.Contains("L")) useCircles = false;
        }
        if(!useCircles){
          if(_Hists.at(i)->_color!=-1){ // user color
            Hists.at(i)->SetMarkerColor(_Hists.at(i)->_color);
            Hists.at(i)->SetLineColor(_Hists.at(i)->_color);
          } else { // default color
            Hists.at(i)->SetMarkerColor(_DefaultColors.at(colorCounter));
            Hists.at(i)->SetLineColor(_DefaultColors.at(colorCounter));
            colorCounter++;
          }
          Hists.at(i)->SetLineWidth(_Hists.at(i)->_lineWidth);
          Hists.at(i)->SetLineStyle(_Hists.at(i)->_lineStyle);
          Hists.at(i)->SetFillStyle(0);
        } else { // Full and Blank circles
          if(_Hists.at(i)->_color!=-1){ // user color
            (i==0) ? Hists.at(i)->SetMarkerColor(_Hists.at(i)->_color) : Hists.at(i)->SetMarkerColor(_Hists.at(i)->_color);
            (i==0) ? Hists.at(i)->SetLineColor(_Hists.at(i)->_color) : Hists.at(i)->SetLineColor(_Hists.at(i)->_color);
          } else { // default color
            (i==0) ? Hists.at(i)->SetMarkerColor(kBlack) : Hists.at(i)->SetMarkerColor(kBlack);
            (i==0) ? Hists.at(i)->SetLineColor(kBlack) : Hists.at(i)->SetLineColor(kBlack);
            colorCounter++;
          }
          (i==0) ? Hists.at(i)->SetMarkerStyle(20) : Hists.at(i)->SetMarkerStyle(24);
          Hists.at(i)->SetFillStyle(0);
        }
      }
    } else { // _Nhists > 2
      if(_Hists.at(i)->_color!=-1){ // user color
        Hists.at(i)->SetMarkerColor(_Hists.at(i)->_color);
        Hists.at(i)->SetLineColor(_Hists.at(i)->_color);
      } else {
        Hists.at(i)->SetMarkerColor(_DefaultColors.at(colorCounter));
        Hists.at(i)->SetLineColor(_DefaultColors.at(colorCounter));
        colorCounter++;
      }
      bool useMarkers = true;
      for(unsigned int j=0;j<_Nhists;++j){
        if(_Hists.at(j)->_drawOption.Contains("HIST") || _Hists.at(j)->_drawOption.Contains("hist") || _Hists.at(j)->_drawOption.Contains("C") || _Hists.at(j)->_drawOption.Contains("L")) useMarkers = false;
      }
      if(!useMarkers){
        Hists.at(i)->SetLineWidth(_Hists.at(i)->_lineWidth);
        Hists.at(i)->SetLineStyle(_Hists.at(i)->_lineStyle);
        Hists.at(i)->SetFillStyle(0);
      } else { // user markers
        Hists.at(i)->SetLineWidth(_Hists.at(i)->_lineWidth);
        Hists.at(i)->SetMarkerStyle(_Markers.at(i));
      }
    }

  } //END: loop over graphs
  if(_Debug) cout << "End setting graphs's user options" << endl;*/


  //----------------------------------
  // Draw Histograms (not Only case)
  //----------------------------------

  if(m_Debug) cout << "Loop over histograms (draw histograms)" << endl;
  if(!m_ComparisonType.Contains("Only")){
    // Create THStack instance
    m_Stack = new THStack("FP","");
    for(unsigned int i=0;i<m_Nhists;++i){ // Loop over histograms
      // Set y-axis range
      HelperFunctions::SetyRange(Hists.at(i),m_Ymin,m_Ymax);
      if(m_Hists.at(i)->DrawOption().Contains("HIST") || m_Hists.at(i)->DrawOption().Contains("hist")) Hists.at(i)->SetFillColorAlpha(kWhite,0);
      // Add histogram to Stack 
      m_Stack->Add(Hists.at(i),m_Hists.at(i)->DrawOption());
    }
    // Draw Stack
    m_Stack->Draw("nostack 0");
    // Set Titles of Stack
    HelperFunctions::SetTitles(m_Stack,m_Xtitle,m_Ytitle,1.3,1.35); // TODO Allow to choose the offset
    // Set x-axis range of Stack
    m_Stack->GetXaxis()->SetRangeUser(m_Xmin,m_Xmax);
    // Set y-axis range of Stack
    if(m_Ymin!=m_Ymax){
      m_Stack->SetMinimum(m_Ymin);
      m_Stack->SetMaximum(m_Ymax);
    }
    if(m_SetMoreLogLabels_x && m_Xlog) m_Stack->GetXaxis()->SetMoreLogLabels();
    if(m_SetMoreLogLabels_y && m_Ylog) m_Stack->GetYaxis()->SetMoreLogLabels();
    if(m_ComparisonType=="Normal"){
      m_Stack->GetXaxis()->SetTitleSize(20);
      m_Stack->GetXaxis()->SetTitleFont(43);
      m_Stack->GetYaxis()->SetLabelFont(43);
      m_Stack->GetYaxis()->SetLabelSize(16);
      if(m_SetMoreLogLabels_x && m_Xlog && m_ComparisonType!="Normal" && !m_ComparisonType.Contains("Only")){
        m_Stack->GetXaxis()->SetLabelFont(42);
        m_Stack->GetXaxis()->SetLabelSize(0.09);
      } else if(m_SetMoreLogLabels_x && m_Xlog && (m_ComparisonType=="Normal" || m_ComparisonType.Contains("Only"))){
        m_Stack->GetXaxis()->SetLabelFont(42);
        m_Stack->GetXaxis()->SetLabelSize(0.04);
        m_Stack->GetYaxis()->SetLabelFont(42);
        m_Stack->GetYaxis()->SetLabelSize(0.04);
      } else{
        m_Stack->GetXaxis()->SetLabelFont(43);
        m_Stack->GetXaxis()->SetLabelSize(16);
      }
    }
    if(m_ComparisonType=="Normal" || m_ComparisonType=="Ratio"){// Draw Lines first Panel 
      for(unsigned int i=0;i<m_Lines.size();++i){
        double minLine = m_Lines.at(i)->getMin();
        double maxLine = m_Lines.at(i)->getMax();
        if(minLine==maxLine){
          if(m_Lines.at(i)->getAxis()=="Y" || m_Lines.at(i)->getAxis()=="y"){
            minLine = m_Xmin;
            maxLine = m_Xmax;
          }
          if(m_Lines.at(i)->getAxis()=="X" || m_Lines.at(i)->getAxis()=="x"){
            minLine = m_Ymin;
            maxLine = m_Ymax;
          }
        }
        m_lineOK = DrawLine(Hists.at(0),m_Lines.at(i)->getAxis(),minLine,maxLine,m_Lines.at(i)->getValue(),m_Lines.at(i)->getColor());
        if(!m_lineOK) return 0;
      }
    }
  } //END: loop over hists

  if(m_Debug) cout << "End of loop" << endl;

  //----------------------------------
  // Set logarithm axis if requested
  //----------------------------------

  if(m_Ylog){
    if(m_Debug) cout << "Setting logarithm y-axis" << endl;
    if(m_SecondPanel) pad1->SetLogy();
    else{can->SetLogy();}
  }
  if(m_Xlog){
    if(m_Debug) cout << "Setting logarithm x-axis" << endl;
    if(m_SecondPanel) pad1->SetLogx();
    else{can->SetLogx();}
  }

  // More Cosmetics
  HelperFunctions::SetgStyle();

  //---------------
  // Draw Legends
  //---------------

  if(m_Debug && !m_NoTLegends) cout << "Draw legends" << endl;
  // Read legends and draw options
  vector<TString> Legends;
  vector<TString> DrawOptions;
  for(unsigned int i=0;i<m_Nhists;++i){
    Legends.push_back(m_Hists.at(i)->Legend());
    DrawOptions.push_back(m_Hists.at(i)->DrawOption());
  }
  if(!m_ComparisonType.Contains("Only")){
    m_legendsOK = DrawLegends(Hists, Legends, DrawOptions, m_SecondPanel, m_Luminosity, m_CME, m_ATLASlabel, m_LegendPosition, m_HorizontalErrors, m_Texts, m_NoTLegends, m_TLegendX1, m_TLegendY1, m_TLegendX2, m_TLegendY2);
    if(!m_legendsOK) return 0;
  }
  if(m_Debug && !m_NoTLegends) cout << "Legends drawn" << endl;

  //------------------------------------------------------------------------
  // Compute ratios, differences or relative differences (not Normal case)
  //------------------------------------------------------------------------

  vector<TH1D*> NewHists;
  TPad *pad2 = nullptr;
  if(m_ComparisonType!="Normal"){

    // Do not draw the Y axis label on the upper plot and redraw a small
    // axis instead, in order to avoid the first label (0) to be clipped.
    if(m_SecondPanel){
      m_Stack->GetXaxis()->SetLabelSize(0.);
      m_Stack->GetXaxis()->SetTitleSize(0.);
    }

    // Lower plot will be in pad2
    can->cd(); // Go back to the main canvas before defining pad2
    pad2 = new TPad("pad2", "pad2", 0, 0.05, 1, 0.4);
    if(m_SecondPanel){
      pad2->SetTopMargin(0.03);
      pad2->SetBottomMargin(0.32);
      pad2->Draw();
      pad2->cd(); // pad2 becomes the current pad
    }

    int NewHistsCount = 0;
    int iNewHist = -1;
    int colorCounter = 0;
    for(unsigned int i=0;i<m_Nhists;++i){ // Loop over histograms

      // Skip reference
      if(m_Hists.at(i)->ID()==m_ReferenceHist){
        if(m_Hists.at(i)->Color()==-1) colorCounter++; // don't use reference color
        continue;
      }

      iNewHist++;

      // New histogram with ratio, difference or relative difference
      TH1D* hNew = (TH1D*)Hists.at(i)->Clone(m_Hists.at(i)->ID()+"_New");

      // Set cosmetics
      if(!m_ComparisonType.Contains("Only")){
        if(m_Nhists<3){
          if(m_Nhists==1){
            if(m_Hists.at(i)->Color()!=-1){ // user color
              hNew->SetMarkerColor(m_Hists.at(i)->Color());
              hNew->SetLineColor(m_Hists.at(i)->Color());
            } else { // default color
              hNew->SetMarkerColor(kBlack);
              hNew->SetLineColor(kBlack);
            }
            hNew->SetLineWidth(m_Hists.at(i)->LineWidth());
            hNew->SetMarkerStyle(20);
            hNew->SetFillStyle(0);
          } else { // m_Nhists==2
            bool useCircles = true;
            for(unsigned int j=0;j<m_Nhists;++j){
              if(m_Hists.at(j)->DrawOption().Contains("HIST") || m_Hists.at(j)->DrawOption().Contains("hist") || m_Hists.at(j)->DrawOption().Contains("C") || m_Hists.at(j)->DrawOption().Contains("L")) useCircles = false;
            }
	    if(!useCircles){
              if(m_Hists.at(i)->Color()!=-1){ // user color
                hNew->SetMarkerColor(m_Hists.at(i)->Color());
                hNew->SetLineColor(m_Hists.at(i)->Color());
              } else { // default color
                hNew->SetMarkerColor(m_DefaultColors.at(colorCounter));
                hNew->SetLineColor(m_DefaultColors.at(colorCounter));
                colorCounter++;
              }
              hNew->SetLineWidth(m_Hists.at(i)->LineWidth());
              hNew->SetFillStyle(0);
            } else {// Full and Blank circles
              if(m_Hists.at(i)->Color()!=-1){ // user color
                (i==0) ? hNew->SetMarkerColor(m_Hists.at(i)->Color()) : hNew->SetMarkerColor(m_Hists.at(i)->Color());
                (i==0) ? hNew->SetLineColor(m_Hists.at(i)->Color()) : hNew->SetLineColor(m_Hists.at(i)->Color());
              } else { // default color
                (i==0) ? hNew->SetMarkerColor(kBlack) : hNew->SetMarkerColor(kBlack);
                (i==0) ? hNew->SetLineColor(kBlack) : hNew->SetLineColor(kBlack);
                colorCounter++;
              }
              (i==0) ? hNew->SetMarkerStyle(20) : hNew->SetMarkerStyle(24);
              hNew->SetFillStyle(0);
            }
          }
        } else {
          if(m_Hists.at(i)->Color()!=-1){ // user color
            hNew->SetMarkerColor(m_Hists.at(i)->Color());
            hNew->SetLineColor(m_Hists.at(i)->Color());
          } else { // default color
            hNew->SetMarkerColor(m_DefaultColors.at(colorCounter));
            hNew->SetLineColor(m_DefaultColors.at(colorCounter));
            colorCounter++;
          }
          bool useMarkers = true;
          for(unsigned int j=0;j<m_Nhists;++j){
            if(m_Hists.at(j)->DrawOption().Contains("HIST") || m_Hists.at(j)->DrawOption().Contains("hist") || m_Hists.at(j)->DrawOption().Contains("C") || m_Hists.at(j)->DrawOption().Contains("L")) useMarkers = false;
          }
          if(!useMarkers){
            hNew->SetLineWidth(m_Hists.at(i)->LineWidth());
            hNew->SetLineStyle(m_Hists.at(i)->LineStyle());
            hNew->SetFillStyle(0);
          } else { // use markers
            hNew->SetLineWidth(m_Hists.at(i)->LineWidth());
            hNew->SetLineStyle(m_Hists.at(i)->LineStyle());
            hNew->SetMarkerStyle(m_Markers.at(i));
          }
        }
      } else{ // m_ComparisonType.Contains("Only")
        if(m_Nhists<4){
          if(m_Nhists==2){
            if(m_Hists.at(i)->Color()!=-1){ // user color
              hNew->SetMarkerColor(m_Hists.at(i)->Color());
              hNew->SetLineColor(m_Hists.at(i)->Color());
            } else { // default color
              hNew->SetMarkerColor(kBlack);
              hNew->SetLineColor(kBlack);
            }
            hNew->SetLineWidth(m_Hists.at(i)->LineWidth());
            hNew->SetMarkerStyle(20);
            hNew->SetFillStyle(0);
          } else { // m_Nhists==3
            bool useCircles = true;
            for(unsigned int j=0;j<m_Nhists;++j){
              if(m_Hists.at(j)->DrawOption().Contains("HIST") || m_Hists.at(j)->DrawOption().Contains("hist") || m_Hists.at(j)->DrawOption().Contains("C") || m_Hists.at(j)->DrawOption().Contains("L")) useCircles = false;
            }
            if(!useCircles){
              if(m_Hists.at(i)->Color()!=-1){ // user color
                hNew->SetMarkerColor(m_Hists.at(i)->Color());
                hNew->SetLineColor(m_Hists.at(i)->Color());
              } else { // default color
                hNew->SetMarkerColor(m_DefaultColors.at(colorCounter));
                hNew->SetLineColor(m_DefaultColors.at(colorCounter));
                colorCounter++;
              }
              hNew->SetLineWidth(m_Hists.at(i)->LineWidth());
              hNew->SetFillStyle(0);
            } else {// Full and Blank circles
              if(m_Hists.at(i)->Color()!=-1){ // user color
                (iNewHist==0) ? hNew->SetMarkerColor(m_Hists.at(i)->Color()) : hNew->SetMarkerColor(m_Hists.at(i)->Color());
                (iNewHist==0) ? hNew->SetLineColor(m_Hists.at(i)->Color()) : hNew->SetLineColor(m_Hists.at(i)->Color());
              } else { // default color
                (iNewHist==0) ? hNew->SetMarkerColor(kBlack) : hNew->SetMarkerColor(kBlack);
                (iNewHist==0) ? hNew->SetLineColor(kBlack) : hNew->SetLineColor(kBlack);
              }
              (iNewHist==0) ? hNew->SetMarkerStyle(20) : hNew->SetMarkerStyle(24);
              hNew->SetFillStyle(0);
            }
          }
        } else {
          if(m_Hists.at(i)->Color()!=-1){ // user color
            hNew->SetMarkerColor(m_Hists.at(i)->Color());
            hNew->SetLineColor(m_Hists.at(i)->Color());
          } else { // default color
            hNew->SetMarkerColor(m_DefaultColors.at(colorCounter));
            hNew->SetLineColor(m_DefaultColors.at(colorCounter));
            colorCounter++;
          }
          bool useMarkers = true;
          for(unsigned int j=0;j<m_Nhists;++j){
            if(m_Hists.at(j)->DrawOption().Contains("HIST") || m_Hists.at(j)->DrawOption().Contains("hist") || m_Hists.at(j)->DrawOption().Contains("C") || m_Hists.at(j)->DrawOption().Contains("L")) useMarkers = false;
          }
          if(!useMarkers){
            hNew->SetLineWidth(m_Hists.at(i)->LineWidth());
            hNew->SetLineStyle(m_Hists.at(i)->LineStyle());
            hNew->SetFillStyle(0);
          } else { // use markers
            hNew->SetLineWidth(m_Hists.at(i)->LineWidth());
            hNew->SetLineStyle(m_Hists.at(i)->LineStyle());
            hNew->SetMarkerStyle(m_Markers.at(iNewHist));
          }
        }
      }

      // Compute ratio, difference or relative difference as appropriate
      if(m_ComparisonType.Contains("Ratio"))      hNew->Divide(Hists.at(m_iRefHist));
      else if(m_ComparisonType.Contains("Relative Difference")){
        hNew->Add(Hists.at(m_iRefHist),-1.);
        hNew->Divide(Hists.at(m_iRefHist));
      } else if(m_ComparisonType.Contains("Difference")) hNew->Add(Hists.at(m_iRefHist),-1.);
 
      // Logarithm x-axis
      if(m_Xlog){
        if(m_SecondPanel) pad2->SetLogx();
        else{can->SetLogx();}
      }

      // Set x-axis range
      HelperFunctions::SetxRange(hNew,m_Xmin,m_Xmax);

      /* TODO
      //if(_fitRatio) h3->Fit("pol0");
      */

      NewHists.push_back(hNew);
      ++NewHistsCount;
    }

    // If y-axis range (second pad) was not provided it will be automatically chosen
    if(m_YminSecondPanel==m_YmaxSecondPanel && !m_ComparisonType.Contains("Only")){
      if(m_Debug) cout << "Setting y-axis range of second plot automatically" << endl;
      double ymin = NewHists.at(0)->GetMinimum();
      double ymax = NewHists.at(0)->GetMaximum();
      for(unsigned int i=1;i<NewHists.size();++i){
        double min = NewHists.at(i)->GetMinimum();
        double max = NewHists.at(i)->GetMaximum();
        if(min < ymin) ymin = min;
        if(max > ymax) ymax = max;
      }
      if(ymin>0) m_YminSecondPanel = ymin-0.05*ymin;
      else{m_YminSecondPanel = ymin+0.05*ymin;}
      if(ymax>0) m_YmaxSecondPanel = ymax+0.05*ymax;
      else{m_YmaxSecondPanel = ymax-0.05*ymax;}
    }

    if(m_Debug){
      cout << "m_YminSecondPanel: " << m_YminSecondPanel << endl;
      cout << "m_YmaxSecondPanel: " << m_YmaxSecondPanel << endl;
    }

    // Get draw options
    vector<TString> NewDrawOptions;
    for(unsigned int i=0;i<m_Nhists;++i){
      if(m_Hists.at(i)->ID()==m_ReferenceHist) continue; // Skip reference histogram
      NewDrawOptions.push_back(m_Hists.at(i)->DrawOption());
    }

    // Create THStack instance
    m_Stack_SP = new THStack("SP","");
    for(unsigned int i=0;i<NewHists.size();++i){
      // Set y-axis range
      if(m_ComparisonType.Contains("Only")) HelperFunctions::SetyRange(NewHists.at(i),m_Ymin,m_Ymax);
      else{HelperFunctions::SetyRange(NewHists.at(i),m_YminSecondPanel,m_YmaxSecondPanel);}
      // Add to Stack
      if(m_ShowErrorsSecondPanel) m_Stack_SP->Add(NewHists.at(i),NewDrawOptions.at(i));
      else {m_Stack_SP->Add(NewHists.at(i),"hist");}
    }

    // Draw Stack
    m_Stack_SP->Draw("nostack 0");
    gPad->RedrawAxis();

    // Set Titles for Stack
    TString yTitle = m_Ytitle;
    if(m_SecondPanel) yTitle = m_YtitleSecondPanel;
    HelperFunctions::SetTitlesSecondPanel(m_Stack_SP,yTitle,m_SetMoreLogLabels_x&&!m_ComparisonType.Contains("Only"));
    if(m_SetMoreLogLabels_x && m_Xlog) m_Stack_SP->GetXaxis()->SetMoreLogLabels();
    m_Stack_SP->GetXaxis()->SetTitle(m_Xtitle);
    if(m_ComparisonType.Contains("Only")){
      m_Stack_SP->GetXaxis()->SetTitleSize(20);
      m_Stack_SP->GetXaxis()->SetTitleFont(43);
      m_Stack_SP->GetXaxis()->SetTitleOffset(1.3);
      if(m_SetMoreLogLabels_y && m_Ylog) m_Stack_SP->GetYaxis()->SetMoreLogLabels();
    }

    // Set x-axis range of Stack
    m_Stack_SP->GetXaxis()->SetRangeUser(m_Xmin,m_Xmax);

    // Set y-axis range of Stack
    if(m_ComparisonType.Contains("Only")){ 
      if(m_Ymin!=m_Ymax){
        m_Stack_SP->SetMinimum(m_Ymin);
        m_Stack_SP->SetMaximum(m_Ymax);
      }
    } else if(m_YminSecondPanel!=m_YmaxSecondPanel){
      m_Stack_SP->SetMinimum(m_YminSecondPanel);
      m_Stack_SP->SetMaximum(m_YmaxSecondPanel);
    }

    // Show lines
    // Automatic line
    double value = 1.;
    if(!m_ComparisonType.Contains("Ratio")) value = 0;
    if(!m_PlotNoAutoLine){
      m_lineOK = DrawLine(NewHists.at(0),"Y",m_Xmin,m_Xmax,value);
      if(!m_lineOK) return 0;
    }
    // User's lines
    if(m_SecondPanel){
      for(unsigned int i=0;i<m_LinesSecondPanel.size();++i){
        double minLine = m_LinesSecondPanel.at(i)->getMin();
        double maxLine = m_LinesSecondPanel.at(i)->getMax();
        if(minLine==maxLine){
          if(m_LinesSecondPanel.at(i)->getAxis()=="Y" || m_LinesSecondPanel.at(i)->getAxis()=="y"){
            minLine = m_Xmin;
            maxLine = m_Xmax;
          }
          if(m_LinesSecondPanel.at(i)->getAxis()=="X" || m_LinesSecondPanel.at(i)->getAxis()=="x"){
            minLine = m_YminSecondPanel;
            maxLine = m_YmaxSecondPanel;
          }
        }
        m_lineOK = DrawLine(NewHists.at(0),m_LinesSecondPanel.at(i)->getAxis(),minLine,maxLine,m_LinesSecondPanel.at(i)->getValue(),m_LinesSecondPanel.at(i)->getColor());
        if(!m_lineOK) return 0;
      }
    } else {
      for(unsigned int i=0;i<m_Lines.size();++i){
        double minLine = m_Lines.at(i)->getMin();
        double maxLine = m_Lines.at(i)->getMax();
        if(minLine==maxLine){
          if(m_Lines.at(i)->getAxis()=="Y" || m_Lines.at(i)->getAxis()=="y"){
            minLine = m_Xmin;
            maxLine = m_Xmax;
          }
          if(m_Lines.at(i)->getAxis()=="X" || m_Lines.at(i)->getAxis()=="x"){
            minLine = m_Ymin;
            maxLine = m_Ymax;
          }
        }
        m_lineOK = DrawLine(NewHists.at(0),m_Lines.at(i)->getAxis(),minLine,maxLine,m_Lines.at(i)->getValue(),m_Lines.at(i)->getColor());
        if(!m_lineOK) return 0;
      }
    }

    // Draw Legends
    if(m_ComparisonType.Contains("Only")){
      vector<TString> NewLegends;
      for(unsigned int i=0;i<m_Nhists;++i){
        if(m_Hists.at(i)->ID()==m_ReferenceHist) continue; // Skip reference histogram
        NewLegends.push_back(m_Hists.at(i)->Legend());
      }
      m_legendsOK = DrawLegends(NewHists, NewLegends, NewDrawOptions, m_SecondPanel, m_Luminosity, m_CME, m_ATLASlabel, m_LegendPosition, m_HorizontalErrors, m_Texts, m_NoTLegends, m_TLegendX1, m_TLegendY1, m_TLegendX2, m_TLegendY2);
      if(!m_legendsOK) return 0;
    }

    if(m_OutFormat!="C") can->Print(outPlot);
    else{can->SaveAs(outPlot);}

  } // ComparisonType not Normal

  if(m_ComparisonType=="Normal"){
    if(m_OutFormat!="C") can->Print(outPlot);
    else {can->SaveAs(outPlot);}
  }

  // Delete objects
  if(m_Debug) cout << "Deleting objects" << endl;
  for(unsigned int i=0;i<m_Nhists;++i) delete Hists.at(i);
  for(unsigned int i=0;i<NewHists.size();++i) delete NewHists.at(i);

  if(m_OutFormat!="C") can->Print(outPlot+"]");

  delete pad1;
  if(pad2!=nullptr) delete pad2;
  delete can;

  cout << outPlot << " has been created" << endl;

  return 1;

} // END: Write()

void Plotter1D::AddHist(TString id, TFile* file, TString name, TString drawOption){
  if(!file){ // Protection
    cout << "Plotter1D::AddHist(): File for ID " << id << " does not exist" << endl;
    m_AddHistResults.push_back(0);
  }
  iHist *hist = new iHist(id,file,name);
  if(drawOption!="NONE") hist->setDrawOption(drawOption);
  hist->setHist(NULL);
  m_Hists.push_back(hist);
  m_AddHistResults.push_back(1);
}

void Plotter1D::AddHist(TString id, TH1D* th1, TString drawOption){
  if(!th1){ // Protection
    cout << "Plotter1D::AddHist(): TH1 for ID " << id << " does not exist" << endl;
    m_AddHistResults.push_back(0);
  }
  iHist *hist = new iHist(id,th1);
  hist->setFile(NULL);
  if(drawOption!="NONE") hist->setDrawOption(drawOption);
  m_Hists.push_back(hist);
  m_AddHistResults.push_back(1);
}

void Plotter1D::AddHist(TString id, TString filename, TString histname, TString drawOption){
  // Open File
  TFile *file = new TFile(filename,"READ");
  if(!file){ // Protection
    cout << "Plotter1D::AddHist(): File for ID " << id << " not found" << endl;
    m_AddHistResults.push_back(0);
  }
  iHist *hist = new iHist(id,file,histname);
  if(drawOption!="NONE") hist->setDrawOption(drawOption);
  hist->setHist(NULL);
  m_Hists.push_back(hist);
  m_AddHistResults.push_back(1);
}

void Plotter1D::AddGraph(TString id, TFile* file, TString name, TString drawOption){
  if(!file){ // Protection
    cout << "Plotter1D::AddGraph(): File for ID " << id << " does not exist" << endl;
    m_AddGraphResults.push_back(0);
  }
  iGraph *graph = new iGraph(id,file,name);
  if(drawOption!="NONE") graph->setDrawOption(drawOption);
  graph->setGraph(NULL);
  m_Graphs.push_back(graph);
  m_AddHistResults.push_back(1);
}

void Plotter1D::AddGraph(TString id, TGraph* graph, TString drawOption){
  if(!graph){ // Protection
    cout << "Plotter1D::AddGraph(): Graph for ID " << id << " does not exist" << endl;
    m_AddGraphResults.push_back(0);
  }
  iGraph *Graph = new iGraph(id,graph);
  Graph->setFile(NULL);
  if(drawOption!="NONE") Graph->setDrawOption(drawOption);
  m_Graphs.push_back(Graph);
  m_AddHistResults.push_back(1);
}

void Plotter1D::AddGraph(TString id, TString filename, TString graphname, TString drawOption){
  // Open File
  TFile *file = new TFile(filename,"READ");
  if(!file){ // Protection
    cout << "Plotter1D::AddGraph(): File for ID " << id << " not found" << endl;
    m_AddGraphResults.push_back(0);
  }
  iGraph *graph = new iGraph(id,file,graphname);
  if(drawOption!="NONE") graph->setDrawOption(drawOption);
  graph->setGraph(NULL);
  m_Graphs.push_back(graph);
  m_AddHistResults.push_back(1);
}

//--------------------------------------
// Functions to set histogram's options
//--------------------------------------

void Plotter1D::SetLegend(TString id, TString legend){
  m_Legends[id] = legend;
}

void Plotter1D::SetTLegendPosition(double x1, double y1, double x2, double y2){
  m_TLegendX1 = x1;
  m_TLegendX2 = x2;
  m_TLegendY1 = y1;
  m_TLegendY2 = y2;
}

void Plotter1D::SetTDirectoryFile(TString id, TString name){
  m_TDirectoryFileNames[id] = name;
}

void Plotter1D::Normalize(TString id, TString type, double factor){
  m_NormTypes[id]   = type;
  m_NormFactors[id] = factor;
}

void Plotter1D::Scale(TString id, double factor){
  m_ScaleFactors[id] = factor;
}

void Plotter1D::Rebin(TString id, int factor){
  m_Rebin[id] = factor;
}

void Plotter1D::DivideByBinWidth(TString id){
  m_DivideByBinWidth.push_back(id); 
}

void Plotter1D::SetDrawOption(TString id, TString option){
  m_DrawOptions[id] = option;
}

void Plotter1D::NoTLegend(TString id){
  m_Legends[id] = "NOLEGEND";
}

void Plotter1D::SetColor(TString id, int color){
  m_UserColors[id] = color;
}

void Plotter1D::SetLineStyle(TString id, int style){
  m_LineStyles[id] = style;
}

void Plotter1D::SetLineWidth(TString id, int width){
  m_LineWidths[id] = width;
}

void Plotter1D::SetMarkerStyle(TString id, int style){
  m_MarkerStyles[id] = style;
}

void Plotter1D::SetMarkerSize(TString id, int size){
  m_MarkerSizes[id] = size;
}

void Plotter1D::Smooth(TString id, int ntimes){
  m_Smooth[id] = ntimes;
}

//-----------------------------------
// Functions to set global variables
//-----------------------------------

void Plotter1D::SetRange(TString axis, double min, double max){
  if(axis=="X" || axis=="x"){
    m_Xmin = min;
    m_Xmax = max;
  }
  else if(axis=="Y" || axis=="y"){
    m_Ymin = min;
    m_Ymax = max;
  }
}

void Plotter1D::SetYRangeSecondPanel(double min, double max){
  m_YminSecondPanel = min;
  m_YmaxSecondPanel = max;
}

void Plotter1D::ApplyFormat(Plotter1D *plotter){
 m_OutputFolder          = plotter->m_OutputFolder;
 m_LegendPosition        = plotter->m_LegendPosition;
 m_OutFormat             = plotter->m_OutFormat;
 m_ATLASlabel            = plotter->m_ATLASlabel;
 m_ComparisonType        = plotter->m_ComparisonType;
 m_Xtitle                = plotter->m_Xtitle;
 m_Ytitle                = plotter->m_Ytitle;
 m_Xlog                  = plotter->m_Xlog;
 m_Ylog                  = plotter->m_Ylog;
 m_SetMoreLogLabels_x    = plotter->m_SetMoreLogLabels_x;
 m_SetMoreLogLabels_y    = plotter->m_SetMoreLogLabels_y;
 m_ReferenceHist         = plotter->m_ReferenceHist;
 m_CME                   = plotter->m_CME;
 m_Luminosity            = plotter->m_Luminosity;
 m_Xmin                  = plotter->m_Xmin;
 m_Xmax                  = plotter->m_Xmax;
 m_Ymin                  = plotter->m_Ymin;
 m_Ymax                  = plotter->m_Ymax;
 m_YminSecondPanel       = plotter->m_YminSecondPanel;
 m_YmaxSecondPanel       = plotter->m_YmaxSecondPanel;
 m_Lines                 = plotter->m_Lines;
 m_LinesSecondPanel      = plotter->m_LinesSecondPanel;
 m_Legends               = plotter->m_Legends;
 m_TDirectoryFileNames   = plotter->m_TDirectoryFileNames;
 m_TDirectoryFileNameAll = plotter->m_TDirectoryFileNameAll;
 m_NormTypes             = plotter->m_NormTypes;
 m_NormFactors           = plotter->m_NormFactors;
 m_ScaleFactors          = plotter->m_ScaleFactors;
 m_Rebin                 = plotter->m_Rebin;
 m_YtitleSecondPanel     = plotter->m_YtitleSecondPanel;
 m_HorizontalErrors      = plotter->m_HorizontalErrors;
 m_DivideByBinWidth      = plotter->m_DivideByBinWidth;
 m_DivideByBinWidthAll   = plotter->m_DivideByBinWidthAll;
 m_Smooth                = plotter->m_Smooth;
 m_SmoothAll             = plotter->m_SmoothAll;
 m_DrawOptions           = plotter->m_DrawOptions;
 m_DrawOptionAll         = plotter->m_DrawOptionAll;
 m_Debug                 = plotter->m_Debug;
 m_RebinAll              = plotter->m_RebinAll;
 m_NormTypeAll           = plotter->m_NormTypeAll;
 m_NormFactorAll         = plotter->m_NormFactorAll;
 m_ScaleFactorAll        = plotter->m_ScaleFactorAll;
 m_NoTLegends            = plotter->m_NoTLegends;
 m_ShowErrorsSecondPanel = plotter->m_ShowErrorsSecondPanel;
 m_PlotNoAutoLine        = plotter->m_PlotNoAutoLine;
 m_UserColors            = plotter->m_UserColors;
 m_LineStyles            = plotter->m_LineStyles;
 m_LineWidths            = plotter->m_LineWidths;
 m_LineWidthAll          = plotter->m_LineWidthAll;
 m_MarkerSizeAll         = plotter->m_MarkerSizeAll;
 m_MarkerStyles          = plotter->m_MarkerStyles;
 m_MarkerSizes           = plotter->m_MarkerSizes;
 m_TLegendX1             = plotter->m_TLegendX1;
 m_TLegendX2             = plotter->m_TLegendX2;
 m_TLegendY1             = plotter->m_TLegendY1;
 m_TLegendY2             = plotter->m_TLegendY2;
}

void Plotter1D::AddText(double x, double y, TString text){
  iLegend *legend = new iLegend(x,y,text);
  m_Texts.push_back(legend);
}

void Plotter1D::PlotLine(TString axis, double value, double min, double max, int color){
  HelperClasses::iLine *line = new iLine(axis,min,max,value,color);
  m_Lines.push_back(line);
}

void Plotter1D::PlotLineSecondPanel(TString axis, double value, double min, double max, int color){
  HelperClasses::iLine *line = new iLine(axis,min,max,value,color);
  m_LinesSecondPanel.push_back(line);
}

int Plotter1D::passProtections(){

  // Check result of AddHist() and AddGrap() functions
  for(unsigned int i=0;i<m_AddHistResults.size();++i){
    if(!m_AddHistResults[i]){
      cout << "Plotter1D::Write() Exiting..." << endl;
      return 0;
    }
  }
  for(unsigned int i=0;i<m_AddGraphResults.size();++i){
    if(!m_AddGraphResults[i]){
      cout << "Plotter1D::Write() Exiting..." << endl;
      return 0;
    }
  }

  // Protection when using constructor meant for format purposes only
  if(m_OutputName=="NONE"){
    cout << "Plotter1D::Write(): You used the constructor meant for format purposes only, Write() should not be called in these cases, exiting" << endl;
    return 0;
  }

  // Protection against not recognised comparison types
  if(m_ComparisonType!="Normal" && m_ComparisonType!="Ratio" && m_ComparisonType!="Ratio Only" && m_ComparisonType!="Relative Difference" && m_ComparisonType!="Relative Difference Only" && m_ComparisonType!="Difference" && m_ComparisonType!="Difference Only" && m_ComparisonType!="RatioOnly" && m_ComparisonType!="RelativeDifference" && m_ComparisonType!="RelativeDifferenceOnly" && m_ComparisonType!="DifferenceOnly"){
    cout << "Plotter1D::Write(): Comparison Type not recognised, exiting" << endl;
    return 0;
  }

  // Only Normal type can be used when using graphs
  if(m_ComparisonType!="Normal" && m_Ngraphs>1){
    cout << "Plotter1D::Write(): Only Normal type is allowed to be used if using graphs, exiting" << endl;
    return 0;
  }

  // Protection when no histogram or graph was added
  if(m_Nhists==0 && m_Ngraphs==0 && m_OutputName!="NONE"){
    cout << "Plotter1D::Write(): No histogram or graph was added, exiting" << endl;
    return 0;
  }

  // Protection: at least 2 histograms in not Normal
  if(m_ComparisonType!="Normal" && m_Nhists<2){
    cout << "Plotter1D::Write(): Not enough number of histograms/graphs, exiting" << endl;
    return 0;
  }

  // Protection: Force unique IDs
  for(unsigned int i=0;i<m_Nhists;++i){// loop over histograms
    for(unsigned int j=0;j<m_Nhists;++j){// loop over histograms
      if(i!=j && m_Hists.at(i)->ID()==m_Hists.at(j)->ID()){
        cout << "Plotter1D::Write(): IDs should be unique, please provide unique IDs, exiting" << endl;
        return 0;
      }
    }
    for(unsigned int j=0;j<m_Ngraphs;++j){// loop over graphs
      if(m_Hists.at(i)->ID()==m_Graphs.at(j)->ID()){
        cout << "Plotter1D::Write(): IDs should be unique, please provide unique IDs, exiting" << endl;
        return 0;
      }
    }
  }
  for(unsigned int i=0;i<m_Ngraphs;++i){// loop over graphs
    for(unsigned int j=0;j<m_Ngraphs;++j){// loop over graphs
      if(i!=j && m_Graphs.at(i)->ID()==m_Graphs.at(j)->ID()){
        cout << "Plotter1D::Write(): IDs should be unique, please provide unique IDs, exiting" << endl;
        return 0;
      }
    }
  }

  return 1;
}

int Plotter1D::passMoreProtections(){
  // Protection: Force Normalization OR Scaling
  for(unsigned int i=0;i<m_Nhists;++i){
    if(m_Hists.at(i)->NormType()!="NONE"){
      if(m_Hists.at(i)->NormType()!="Factor" && m_Hists.at(i)->NormType()!="Integral"){
        cout << "Plotter1D::Write(): Normalization type for " << m_Hists.at(i)->ID() << " not recognized, exiting" << endl;
        return 0;
      }
      if(m_Hists.at(i)->ScaleFactor()!=1.){
        cout << "Plotter1D::Write(): Choose one, normalization or scaling, exiting" << endl;
        return 0;
      }
    }
  }
  return 1;
}


int Plotter1D::findReferenceHist(){
  // Find number of the reference histogram
  for(unsigned int i=0;i<m_Nhists;++i){
   if(m_Hists.at(i)->ID() == m_ReferenceHist){ // ID found
     m_iRefHist = i;
     return 1;
   }
  }
  return 0; // provided reference does not match to any ID
}

void Plotter1D::readOptions(){ // Read provided options for each histogram
  // Loop over all available histograms and set options
  for(unsigned int i=0;i<m_Nhists;++i){
    // Set Legend
    if(m_Legends.find(m_Hists.at(i)->ID()) != m_Legends.end()){ // Legend exists
      m_Hists.at(i)->setLegend(m_Legends[m_Hists.at(i)->ID()]);
    }
    // Set TDirectoryFile
    if(m_TDirectoryFileNames.find(m_Hists.at(i)->ID()) != m_TDirectoryFileNames.end()){ // TDirectoryFileName exists
      m_Hists.at(i)->setDirectoryName(m_TDirectoryFileNames[m_Hists.at(i)->ID()]);
    } else if(m_TDirectoryFileNameAll!=""){
      m_Hists.at(i)->setDirectoryName(m_TDirectoryFileNameAll);
    }
    // Set Normalization and Scaling
    if(m_NormTypes.find(m_Hists.at(i)->ID()) != m_NormTypes.end()){
      m_Hists.at(i)->setNormType(m_NormTypes[m_Hists.at(i)->ID()]);
      m_Hists.at(i)->setNormFactor(m_NormFactors[m_Hists.at(i)->ID()]);
    } else if(m_NormTypeAll!=""){
      m_Hists.at(i)->setNormType(m_NormTypeAll);
      m_Hists.at(i)->setNormFactor(m_NormFactorAll);
    }
    if(m_ScaleFactors.find(m_Hists.at(i)->ID()) != m_ScaleFactors.end()){
      m_Hists.at(i)->setScaleFactor(m_ScaleFactors[m_Hists.at(i)->ID()]);
    } else if(m_ScaleFactorAll!=1.){
      m_Hists.at(i)->setScaleFactor(m_ScaleFactorAll);
    }
    // Set Rebin
    if(m_Rebin.find(m_Hists.at(i)->ID()) != m_Rebin.end()){ // Rebin exists
      m_Hists.at(i)->setReBin(m_Rebin[m_Hists.at(i)->ID()]);
    } else if(m_RebinAll!=1){
      m_Hists.at(i)->setReBin(m_RebinAll);
    }
    // Set DevideByBinWidth
    if(m_DivideByBinWidthAll) m_Hists.at(i)->setDivideBinByBinWidth();
    else {
      for(unsigned int j=0;j<m_DivideByBinWidth.size();++j){
        if(m_DivideByBinWidth.at(j) == m_Hists.at(i)->ID()){
          m_Hists.at(i)->setDivideBinByBinWidth();
        }
      }
    }
    // Smooth
    if(m_Smooth.find(m_Hists.at(i)->ID()) != m_Smooth.end()){
      m_Hists.at(i)->setSmooth(m_Smooth[m_Hists.at(i)->ID()]);
    } else if(m_SmoothAll!=0){
      m_Hists.at(i)->setSmooth(m_SmoothAll);
    }
    // Set Draw option
    if(m_Hists.at(i)->DrawOption()=="NONE"){
      if(m_DrawOptions.find(m_Hists.at(i)->ID()) != m_DrawOptions.end()){ // Draw Option exists
        m_Hists.at(i)->setDrawOption(m_DrawOptions[m_Hists.at(i)->ID()]);
      } else if (m_DrawOptionAll!=""){
        m_Hists.at(i)->setDrawOption(m_DrawOptionAll);
      } else { // default
        m_Hists.at(i)->setDrawOption("E");
      }
    }
    // Set Color
    if(m_UserColors.find(m_Hists.at(i)->ID()) != m_UserColors.end()){ // color exists
      m_Hists.at(i)->setColor(m_UserColors[m_Hists.at(i)->ID()]);
    }
    // Set Line Style
    if(m_LineStyles.find(m_Hists.at(i)->ID()) != m_LineStyles.end()){ // line style exists
      m_Hists.at(i)->setLineStyle(m_LineStyles[m_Hists.at(i)->ID()]);
    }
    // Set Line Width
    if(m_LineWidthAll!=-1) m_Hists.at(i)->setLineWidth(m_LineWidthAll);
    else if(m_LineWidths.find(m_Hists.at(i)->ID()) != m_LineWidths.end()){ // line width exists
      m_Hists.at(i)->setLineWidth(m_LineWidths[m_Hists.at(i)->ID()]);
    }
  }//END: loop over histograms
  // Loop over all available graphs and set options
  for(unsigned int i=0;i<m_Ngraphs;++i){
    // Set Legend
    if(m_Legends.find(m_Graphs.at(i)->ID()) != m_Legends.end()){ // Legend exists
      m_Graphs.at(i)->setLegend(m_Legends[m_Graphs.at(i)->ID()]);
    }
    // Set TDirectoryFile
    if(m_TDirectoryFileNames.find(m_Graphs.at(i)->ID()) != m_TDirectoryFileNames.end()){ // TDirectoryFileName exists
      m_Graphs.at(i)->setDirectoryName(m_TDirectoryFileNames[m_Graphs.at(i)->ID()]);
    } else if(m_TDirectoryFileNameAll!=""){
      m_Graphs.at(i)->setDirectoryName(m_TDirectoryFileNameAll);
    }
    // Set Draw option
    if(m_Graphs.at(i)->DrawOption()=="NONE"){
      if(m_DrawOptions.find(m_Graphs.at(i)->ID()) != m_DrawOptions.end()){ // Draw Option exists
        m_Graphs.at(i)->setDrawOption(m_DrawOptions[m_Graphs.at(i)->ID()]);
      } else if (m_DrawOptionAll!=""){
        m_Graphs.at(i)->setDrawOption(m_DrawOptionAll);
      } else { // default
        m_Graphs.at(i)->setDrawOption("P");
      }
    }
    // Set Color
    if(m_UserColors.find(m_Graphs.at(i)->ID()) != m_UserColors.end()){ // color exists
      m_Graphs.at(i)->setColor(m_UserColors[m_Graphs.at(i)->ID()]);
    }
    // Set Line Style
    if(m_LineStyles.find(m_Graphs.at(i)->ID()) != m_LineStyles.end()){ // line style exists
      m_Graphs.at(i)->setLineStyle(m_LineStyles[m_Graphs.at(i)->ID()]);
    }
    // Set Line Width
    if(m_LineWidthAll!=-1) m_Graphs.at(i)->setLineWidth(m_LineWidthAll);
    else if(m_LineWidths.find(m_Graphs.at(i)->ID()) != m_LineWidths.end()){ // line width exists
      m_Graphs.at(i)->setLineWidth(m_LineWidths[m_Graphs.at(i)->ID()]);
    }
    // Set Marker Style
    if(m_MarkerStyles.find(m_Graphs.at(i)->ID()) != m_MarkerStyles.end()){ // marker style exists
      m_Graphs.at(i)->setMarkerStyle(m_MarkerStyles[m_Graphs.at(i)->ID()]);
    }
    // Set Marker Size
    if(m_MarkerSizeAll!=-1) m_Graphs.at(i)->setMarkerSize(m_MarkerSizeAll);
    else if(m_MarkerSizes.find(m_Graphs.at(i)->ID()) != m_MarkerSizes.end()){ // marker size exists
      m_Graphs.at(i)->setMarkerSize(m_MarkerSizes[m_Graphs.at(i)->ID()]);
    }
  }//END: loop over histograms
}

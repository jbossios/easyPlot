#include <easyPlot/includes_and_typedefs.h>
#include <easyPlot/HelperFunctions.h>
#include <easyPlot/HelperClasses.h>
#include <easyPlot/Plotter1D.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#endif

#ifdef __CINT__
#pragma link C++ class Plotter1D;
#endif

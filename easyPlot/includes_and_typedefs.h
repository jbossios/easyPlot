#include <sstream>
#include <string>
#include <map>
#include <sstream>
#include <string>
#include <vector>
#include <string>
#include <boost/type_index.hpp>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <algorithm>
#include <utility>
#include <math.h>
#include <stdio.h>

// ROOT includes
#include <TROOT.h>
#include "TLatex.h"
#include "TLorentzVector.h"
#include "TFile.h"
#include "TTree.h"
#include "TMathText.h"
#include "TCanvas.h"
#include "TChain.h"
#include "TFile.h"
#include "TH2.h"
#include "TStyle.h"
#include "TString.h"
#include "TStopwatch.h"
#include "TMath.h"
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TLine.h"
#include "TH1.h"
#include "TMathBase.h"
#include "TLegend.h"
#include "TColor.h"
#include "TMarker.h"
#include "THStack.h"

typedef std::vector<TString> vStr;
typedef std::vector<TH1D*>   vTh1;
typedef std::vector<TGraph*> vGph;

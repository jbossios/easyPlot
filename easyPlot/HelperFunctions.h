#ifndef HELPERFUNCTIONS_H
#define HELPERFUNCTIONS_H

#include "includes_and_typedefs.h"
#include "easyPlot/HelperClasses.h"

using namespace std;
  
namespace HelperFunctions{
  void SetxRange(TH1D*, double,double);
  void SetxRange(TGraph*, double,double);
  void SetxRangeNew(TH1D*, double&,double&);
  void SetyRange(TH1D*, double,double);
  void SetyRange(TGraphAsymmErrors*,double,double);
  void SetgStyle();
  void Rebin(TH1D*, int);
  void SetTitles(THStack*, TString, TString, double xOffset=-1, double yOffset=-1);
  void SetTitlesSecondPanel(THStack*, TString, bool);
  void DivideBinbyBin(TH1D*);
  int  DrawLine(TH1D*, TString, double, double, double,int color=-1);
  int  DrawLegends(vTh1, vStr, vStr, bool, TString, TString, TString, TString, bool, vector<HelperClasses::iLegend*>, bool, double, double, double, double);
  int  GetLength(TString);

  //--------------------------------------------------------------------------------- 
  // Get all histograms or graphs 
  // Get info from class iHist/iGraph and put all hists/graphs into a vector
  //--------------------------------------------------------------------------------- 
  template <class T, typename inputType> // examples: <iHist,TH1D>, <iGraph,TGraph>
  int GetInputs(vector<T*> inputs, vector<inputType*>& Inputs){
    TDirectory* TDir = NULL;
    for(unsigned int i=0;i<inputs.size();++i){ // Loop over added histograms/graphs
      inputType* Temp  = NULL;
      inputType* Input = NULL;
      TDir             = NULL;
      // Get histogram/graph from TFile
      if(inputs.at(i)->File()){// if TFile exists, get the histogram/graph
        if(inputs.at(i)->DirectoryName()=="") Temp = (inputType*)inputs.at(i)->File()->Get(inputs.at(i)->Name());
        else {
          // Get directory
          TDir = inputs.at(i)->File()->GetDirectory(inputs.at(i)->DirectoryName());
          if(!TDir){ // Protection
            cout << "Plotter1D::Write(): TDirectoryFile named " << inputs.at(i)->DirectoryName() << " was not found, exiting" << endl;
            return 0;
          }
          // get histogram/graph
          TDir->GetObject(inputs.at(i)->Name(),Temp);
        }
        Input = (inputType*)Temp->Clone(inputs.at(i)->ID()+"_clone");
        delete Temp;
      } else if(inputs.at(i)->Object()) {// Clone provided histogram/graph
        Input = (inputType*)inputs.at(i)->Object()->Clone(inputs.at(i)->ID()+"_clone");
      }
      if(!Input){ // Protection
        cout << "Plotter1D::Write(): " << inputs.at(i)->Name() << " was not found, exiting" << endl;
        return 0;
      }
      Inputs.push_back(Input);
    }
    return 1;
  } 

}

#endif // HELPERFUNCTIONS_H

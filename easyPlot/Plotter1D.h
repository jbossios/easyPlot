#ifndef PLOTTER1D_H
#define PLOTTER1D_H

#include "includes_and_typedefs.h"

namespace HelperClasses {
#ifndef HELPERCLASSES_ILINE_H 
	  class iLine;
#endif
#ifndef HELPERCLASSES_IHIST_H 
	  class iHist;
#endif
#ifndef HELPERCLASSES_IGRAPH_H 
	  class iGraph;
#endif
#ifndef HELPERCLASSES_ILEGEND_H
	  class iLegend;
#endif
}

class Plotter1D {

 private:

  std::vector<HelperClasses::iHist*>  m_Hists;
  std::vector<HelperClasses::iGraph*> m_Graphs;
  std::vector<bool>                  m_AddHistResults;
  std::vector<bool>                  m_AddGraphResults;
  TString m_OutputFolder       = "NONE";
  TString m_OutputName         = "NONE"; // Default will not print any plot (only used as format)
  TString m_LegendPosition     = "TopRight"; // options: Top, TopRight, TopLeft, Bottom
  TString m_OutFormat          = "PDF"; // options: PDF (default), PNG, EPS, C
  TString m_ATLASlabel         = "Internal"; // options: NONE, ATLAS, Internal, WIP, Preliminary, Simulation, SimulationInternal, SimulationWIP, SimulationPreliminary
  TString m_ComparisonType     = "Normal"; // other options: Ratio, Ratio Only, Relative Difference, Relative Difference Only, Difference, Difference Only
  TString m_Xtitle             = ""; // x-axis title
  TString m_Ytitle             = ""; // y-xis title
  TString m_ReferenceHist      = ""; // ID of reference histogram (only when m_SecondPanel is true)
  TString m_YtitleSecondPanel  = "";
  TString m_CME                = "-999";
  TString m_Luminosity         = "-999";
  bool m_HorizontalErrors      = false; // The horizontal error bars are not shown by default
  bool m_Xlog                  = false;
  bool m_Ylog                  = false;
  bool m_Debug                 = false;
  bool m_SecondPanel           = false; // Set to true in Write() for appropriate m_ComparisonType cases
  bool m_SetMoreLogLabels_x    = false; // Set more log labels in x-axis
  bool m_SetMoreLogLabels_y    = false; // Set more log lables in y_axis
  bool m_NoTLegends            = false; // TLegend is shown by default
  bool m_ShowErrorsSecondPanel = true;
  bool m_PlotNoAutoLine        = false;
  bool m_lineOK                = false;
  bool m_legendsOK             = false;
  unsigned int m_iRefHist      = 0;
  unsigned int m_iRefGraph     = 0;
  unsigned int m_Nhists        = 0;
  unsigned int m_Ngraphs       = 0;
  double  m_Xmin       = 1.;
  double  m_Xmax       = 1.;
  double  m_Ymin       = 1.;
  double  m_Ymax       = 1.;
  double  m_YminSecondPanel = 1.;
  double  m_YmaxSecondPanel = 1.;
  double  m_TLegendX1       = -1;
  double  m_TLegendX2       = -1;
  double  m_TLegendY1       = -1;
  double  m_TLegendY2       = -1;
  std::vector<HelperClasses::iLine*>    m_Lines; // First Panel
  std::vector<HelperClasses::iLine*>    m_LinesSecondPanel;
  std::vector<HelperClasses::iLegend*> m_Texts;
  std::vector<TString>                 m_DivideByBinWidth;
  std::map<TString,TString>            m_Legends;
  std::map<TString,TString>            m_TDirectoryFileNames;
  std::map<TString,TString>            m_DrawOptions;
  std::map<TString,TString>            m_NormTypes;
  std::map<TString,double>             m_NormFactors;
  std::map<TString,double>             m_ScaleFactors;
  std::map<TString,int>                m_Rebin;
  std::map<TString,int>                m_UserColors;   // provided colors
  std::map<TString,int>                m_LineStyles;   // provided line styles
  std::map<TString,int>                m_LineWidths;   // provided line widths
  std::map<TString,int>                m_MarkerStyles; // provided line styles
  std::map<TString,int>                m_MarkerSizes;  // provided line widths
  std::map<TString,int>                m_Smooth;
  std::vector<int>                     m_DefaultColors={kBlack,kRed+2,kGreen+2,kMagenta+1,kOrange+2,kAzure+10,kBlue+2,kBlack,kBlack,kBlack,kBlack,kBlack,kBlack,kBlack};
  std::vector<int>                     m_Markers={21,20,22,23,34,33,29,24,25,26,27,28,30,31};
  THStack *m_Stack    = nullptr;    // Used only when _SeconPanel is true
  THStack *m_Stack_SP = nullptr; // Used always except Normal case 

  TString m_TDirectoryFileNameAll = "";
  TString m_DrawOptionAll         = "";
  int     m_RebinAll              = 1;
  int     m_LineWidthAll          = -1;
  int     m_MarkerSizeAll         = -1;
  int     m_SmoothAll             = 0;
  bool    m_DivideByBinWidthAll   = false;
  TString m_NormTypeAll           = "";
  double  m_NormFactorAll         = 1;
  double  m_ScaleFactorAll        = 1.;

  // Functions
  int  passProtections();
  int  passMoreProtections();
  int  findReferenceHist();
  void readOptions();

  /* TO IMPLEMENT LATER
  bool _fitRatio;
  bool _binomialErrors;
  */

 public:

  Plotter1D(TString);
  Plotter1D();        // For format purpose
  ~ Plotter1D();

  // Main function

  int Write(); // Produces plot and saves it

  // Setting global variables

  void AddHist(TString id, TFile* file, TString name, TString drawOption="NONE");
  void AddHist(TString id, TH1D* th1, TString drawOption="NONE");
  void AddHist(TString id, TString filename, TString histname, TString drawOption="NONE");
  void AddGraph(TString id, TFile* file, TString name, TString drawOption="NONE");
  void AddGraph(TString id, TGraph* graph, TString drawOption="NONE");
  void AddGraph(TString id, TString filename, TString graphname, TString drawOption="NONE");
  inline void ShowLuminosity(TString lumi){m_Luminosity=lumi;}
  inline void ShowLumi(TString lumi){ShowLuminosity(lumi);}
  inline void ShowCME(TString cme){m_CME=cme;}
  inline void SetOutputFolder(TString outfolder){m_OutputFolder=outfolder;}
  inline void Debug(){m_Debug=true;}
  inline void SetLogx(){m_Xlog=true;}
  inline void SetLogy(){m_Ylog=true;}
  inline void UnSetLogx(){m_Xlog=false;}
  inline void UnSetLogy(){m_Ylog=false;}
  inline void UnSetLogs(){m_Xlog=false;m_Ylog=false;}
  inline void SetFormat(TString format){m_OutFormat=format;}
  inline void SetATLASlabel(TString label){m_ATLASlabel=label;}  
  inline void SetAxisTitles(TString x, TString y){m_Xtitle=x;m_Ytitle=y;}
  inline void SetComparisonType(TString type){m_ComparisonType=type;}
  void SetRange(TString,double,double); // axis, min, max
  inline void SetXRange(double min,double max){SetRange("X",min,max);}
  inline void SetYRange(double min,double max){SetRange("Y",min,max);}
  void SetYRangeSecondPanel(double, double);
  inline void SetMoreLogLabelsX(){m_SetMoreLogLabels_x=true;}
  inline void SetMoreLogLabelsY(){m_SetMoreLogLabels_y=true;}
  inline void SetMoreLogLabels() {m_SetMoreLogLabels_x=true;m_SetMoreLogLabels_y=true;}
  inline void UnSetMoreLogLabels(){m_SetMoreLogLabels_x=false;m_SetMoreLogLabels_y=false;}
  inline void SetReferenceHistSecondPanel(TString id){m_ReferenceHist=id;}
  inline void SetReferenceSecondPanel(TString id){m_ReferenceHist=id;}// to maintain backwards compatibility
  inline void SetLegendPosition(TString pos){m_LegendPosition=pos;}
  void SetTLegendPosition(double,double,double,double);
  inline void SetYTitleSecondPanel(TString title){m_YtitleSecondPanel=title;}
  inline void ShowHorizontalErrors(){m_HorizontalErrors=true;}
  inline void NoTLegends(){m_NoTLegends = true;}
  inline void ShowNoErrorsSecondPanel(){m_ShowErrorsSecondPanel = false;}
  inline void PlotNoAutoLine(){m_PlotNoAutoLine = true;}
  void AddText(double,double,TString);
  void ApplyFormat(Plotter1D*);
  void PlotLine(TString, double, double min=-999, double max=-999, int color=-1);
  void PlotLineSecondPanel(TString, double, double min=-999, double max=-999, int color=-1);

  // Setting histograms' and graphs' options
 
  // Options for an individual histogram or graph (given ID)
  void SetLegend(TString,TString);                 // ID, legend
  void SetTDirectoryFile(TString,TString);         // ID, directoryName
  void SetDrawOption(TString,TString);             // ID, option
  void NoTLegend(TString);                         // ID
  void SetColor(TString,int);                      // ID, color
  void SetLineStyle(TString,int);                  // ID, style
  void SetLineWidth(TString,int);                  // ID, width
  // Options for an individual histogram (given ID)
  void Normalize(TString,TString,double factor=1); // ID, normalizationType, factor
  void Scale(TString,double);                      // ID, scaleFactor
  void Rebin(TString,int);                         // ID, factor
  void DivideByBinWidth(TString);                  // ID
  void Smooth(TString,int ntimes=1);               // ID, ntimes
  // Options for an individual graph (given ID)
  void SetMarkerStyle(TString,int);                // ID, style
  void SetMarkerSize(TString,int);                 // ID, size
 
  // Options for all histograms and graphs
  inline void SetTDirectoryFileAll(TString name){m_TDirectoryFileNameAll = name;}
  inline void SetDrawOptionAll(TString option){m_DrawOptionAll=option;}
  inline void SetLineWidthAll(int width){m_LineWidthAll=width;}
  // Options for all histograms
  inline void NormalizeAll(TString type,double factor=1){m_NormTypeAll=type;m_NormFactorAll=factor;}
  inline void ScaleAll(double factor){m_ScaleFactorAll=factor;}
  inline void RebinAll(int rebin){m_RebinAll=rebin;}
  inline void DivideByBinWidthAll(){m_DivideByBinWidthAll=true;}
  inline void SmoothAll(int ntimes=1){m_SmoothAll=ntimes;}
  // Options for all graphs
  inline void SetMarkerSizeAll(int size){m_MarkerSizeAll=size;}

  /* TO IMPLEMENT LATER 
  void FitRatio();
  void BinomialErrors();
  */

};

#endif // PLOTTER1D_H

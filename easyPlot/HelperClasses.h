#ifndef HELPERCLASSES_H
#define HELPERCLASSES_H

#include "includes_and_typedefs.h"

namespace HelperClasses{

  // Base class for iHist and iGraph
  class iBaseObject{

   private:

    TFile*  m_File;
    TString m_ID;
    TString m_Name;
    TString m_Legend;
    TString m_DirectoryName       = "";
    TString m_DrawOption          = "NONE";    // options: HIST, P, E, L, C, etc. See: https://root.cern/doc/v610/classTHistPainter.html
    int     m_Color               = -1;
    int     m_LineStyle           = 1;
    int     m_LineWidth           = 3;

    // FIXME put Object function here and use a template

   public:

    // Constructors
    iBaseObject(TString);                // ID,
    iBaseObject(TString,TFile*,TString); // ID, TFile, Name

    inline void    setFile(TFile *file){m_File=file;}
    inline TFile*  File(){return m_File;}
    inline void    setID(TString id){m_ID=id;}
    inline TString ID(){return m_ID;}
    inline TString Name(){return m_Name;}
    inline void    setLegend(TString legend){m_Legend=legend;}
    inline TString Legend(){return m_Legend;}
    inline void    setDirectoryName(TString dirname){m_DirectoryName=dirname;}
    inline TString DirectoryName(){return m_DirectoryName;}
    inline void    setDrawOption(TString option){m_DrawOption=option;}
    inline TString DrawOption(){return m_DrawOption;}
    inline void    setColor(int color){m_Color=color;}
    inline int     Color(){return m_Color;}
    inline void    setLineStyle(int style){m_LineStyle=style;}
    inline int     LineStyle(){return m_LineStyle;}
    inline void    setLineWidth(int width){m_LineWidth=width;}
    inline int     LineWidth(){return m_LineWidth;}

  };

  // Class for handling histograms
  class iHist : public iBaseObject{

   private:

    TH1D*   m_Hist;
    TString m_NormType            = "NONE"; // options: Factor, Integral
    double  m_NormFactor          = 1.;
    double  m_ScaleFactor         = 1.;
    bool    m_DivideBinByBinWidth = false;
    int     m_ReBin               = 1;
    int     m_Smooth              = 0;

   public:

    // Constructors
    iHist(TString,TH1D*);          // ID, TH1D
    iHist(TString,TFile*,TString); // ID, TFile, Name

    inline void    setHist(TH1D* hist){m_Hist=hist;}
    inline TH1D*   Object(){return m_Hist;}
    inline void    setNormType(TString normtype){m_NormType=normtype;}
    inline TString NormType(){return m_NormType;}
    inline void    setNormFactor(double factor){m_NormFactor=factor;}
    inline double  NormFactor(){return m_NormFactor;}
    inline void    setScaleFactor(double factor){m_ScaleFactor=factor;}
    inline double  ScaleFactor(){return m_ScaleFactor;}
    inline void    setDivideBinByBinWidth(){m_DivideBinByBinWidth=true;}
    inline bool    DivideBinByBinWidth(){return m_DivideBinByBinWidth;}
    inline void    setReBin(int factor){m_ReBin=factor;}
    inline int     ReBin(){return m_ReBin;}
    inline void    setSmooth(int option){m_Smooth=option;}
    inline int     Smooth(){return m_Smooth;}

  };

  // Class for handling graphs
  class iGraph : public iBaseObject{

   private:

    TGraph* m_Graph;
    int     m_MarkerStyle         = 1;
    int     m_MarkerSize          = 3;

   public:

    // Constructors
    iGraph(TString,TGraph*);        // ID, TGraph
    iGraph(TString,TFile*,TString); // ID, TFile, Name

    inline void    setGraph(TGraph* graph){m_Graph=graph;}
    inline TGraph* Object(){return m_Graph;}
    inline void    setMarkerStyle(int style){m_MarkerStyle=style;}
    inline int     MarkerStyle(){return m_MarkerStyle;}
    inline void    setMarkerSize(int size){m_MarkerSize=size;}
    inline int     MarkerSize(){return m_MarkerSize;}

  };


  // Class for handling TLegends
  class iLegend{

    private:

      double  m_X;
      double  m_Y;
      TString m_Text;

    public:

      // Constructor
      iLegend(double,double,TString); // x,y,text

      inline double  getX(){return m_X;}
      inline double  getY(){return m_Y;}
      inline TString getText(){return m_Text;}
  
  };

  // Class for handling TLines
  class iLine{

   private:

    TString m_Axis;
    double  m_Min;
    double  m_Max; 
    double  m_Value; 
    int     m_Color;

   public:
   
    // Constructor 
    iLine(TString,double,double,double,int color=-1);

    inline TString getAxis(){return m_Axis;}
    inline double  getMin(){return m_Min;}
    inline double  getMax(){return m_Max;}
    inline double  getValue(){return m_Value;}
    inline int     getColor(){return m_Color;}
    
  };

}

#endif // HELPERCLASSES_H
